<html>
<body>
<table border="1px" style="width:50%">
    <tr>
        <th>Serial</th>
        <th>ID</th>
        <th>Book Title</th>
        <th>Action</th>
    </tr>
    <tr>
        <td>1</td>
        <td>5</td>
        <td>PHP- BOOK TITLE #1</td>
        <td>
            <a href="view.php"><button>View</button></a>
            <a href="edit.php"><button type="button">Edit</button></a>
            <a href="delete.php"><button type="button">Delete</button></a>
            <a href="trash.php"><button type="button">Trash</button></a>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>6</td>
        <td>PHP- BOOK TITLE #2</td>
        <td>
            <button type="button">View</button>
            <button type="button">Edit</button>
            <button type="button">Delete</button>
            <button type="button">Trash</button>
        </td>
    </tr>
    <tr>
        <td>2</td>
        <td>7</td>
        <td>PHP- BOOK TITLE #3</td>
        <td>
            <button type="button">View</button>
            <button type="button">Edit</button>
            <button type="button">Delete</button>
            <button type="button">Trash</button>
        </td>
    </tr>
    <tr>
        <td colspan="4">PREVIOUS: <1,2,3,4,5,6,7> NEXT</td>
    </tr>
    <tr>
        <td colspan="4">
            <button type="button">Add new book title</button>
            <button type="button">View Trash Items</button>
            <button type="button">Download as PDF</button>
            <button type="button">Download as excel file</button>
        </td>
    </tr>
</table>
</body>
</html>