<html xmlns="http://www.w3.org/1999/html">
    <head>

        <meta charset="utf-8">
        <title>Different headings</title>

    </head>
    <body>

    <h1>Hello world</h1>
    <h2>Hello world</h2>
    <h3>Hello world</h3>
    <h4>Hello world</h4>
    <h5>Hello world</h5>
    <h6>Hello world</h6>

    <p>This is large text but this is a <small> small</small> text.</p>
    <p>Nullamquisrisusegeturnamollisornareveleuleo. Cum sociisnatoquepenatibusetmagnis dis parturient montes, nasceturridiculus mus.
        Nullam id dolor id nibhultriciesvehicula. Nullamquisrisusegeturnamollisornareveleuleo. Cum sociisnatoquepenatibusetmagnis
        dis parturient montes, nasceturridiculus mus. Nullam id dolor id nibhultriciesvehicula.
    </p>
    <p>
        Cum sociisnatoquepenatibusetmagnis dis parturient montes, nasceturridiculus mus. Donecullamcorpernulla non metusauctorfringilla. Duismollis, est non commodoluctus, nisi eratporttitor ligula, egetlaciniaodiosemnecelit. Donecullamcorpernulla non metusauctorfringilla. Nullamquisrisusegeturnamollisornareveleuleo.
        Cum sociisnatoquepenatibusetmagnis dis parturient montes, nasceturridiculus mus. Nullam id dolor id nibhultriciesvehicula.
    </p>
    <p>
        Maecenas seddiamegetrisusvariusblandit sit amet non magna. Donec id elit non mi portagravida at egetmetus.
        Duismollis, est non commodoluctus, nisi eratporttitor ligula, egetlaciniaodiosemnecelit.
    </p>

    <p>
        We can use a mark tag to<mark> highlight</mark> a text.<br>
        Some text to show how to <del>delete</del>  some text.<br>
        <u>This line is underlined</u><br>
        <b>This is a bold text</b><br>
        <i>This is an italic tag.</i><br>
        <br>
        this is a long text:
        <blockquote>
        This is a blockquote text example.This is a blockquote text example.This is a blockquote text example.
        </blockquote>

        The <abbr title="World Heath Organization">WHO</abbr> was found in 1998.<br>
        Do the work <acronym title="As soon as possible">ASAP.</acronym><br>
        <br>
        <address>
            Written by Sajeda Yeasmin.<br>
            <a href="#">Email us:</a>
            Address: House#16,Road#12,Nikunjo<br>
            Phone: 01681662649
        </address>
        <bdo dir="rtl">
        this is a bdo tag.<br>
        </bdo>
        This text is <big>big</big> but this is not.<br>
        <cite>This Is a Citation</cite><br>
        this is some text<code>This is a code.</code>this is some text.<br>
        <dfn id="dfn_id">The Internet</dfn> is a global system of interconnected networks that use the Internet Protocol Suite (TCP/IP)
        to serve billions of users worldwide.<br>
        I am <del>very</del> <ins>extremely</ins> happy that you visited the page.
        <pre>
            some random generated text.
            some random generated text.
            some random generated text.
            some random generated text.
        </pre>
        Chemical structure of water is h<sub>2</sub>O.<br>
        2<sup>2</sup>=4.
    </p>
    </body>
</html>